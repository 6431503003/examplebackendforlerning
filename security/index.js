const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");
const crypto = require("crypto-js");
// const { validate } = require("uuid");

dotenv.config();

module.exports = {
  passwordGenerator(password) {
    const hash = crypto.SHA256(password);
    return hash.toString(crypto.enc.Hex);
  },
  tokenGenerator(data) {
    return jwt.sign({ data: data }, process.env.SECRET_TOKEN, { expiresIn: "7d" });
  },
  validUser(req, res, next) {
    let header = req.headers["authorization"];
    let token = header && header.split(" ")[1];
    jwt.verify(token, process.env.TOKEN, (err, data) => {
      if (err) res.status(401).send({ message: "Token expired" });

      req.user = data;

      next();
    });
  },

  reactiveUser(req, res, next) {
    let header = req.headers["authorization"];
    let token = header && header.split(" ")[1];
    if (token === undefined) {
      res.status(400).send({ message: "Token not correct" });
    }

    let dataFromToken = jwt.decode(token, process.env.TOKEN);

    if (dataFromToken === null)
      res.status(500).send({ message: "Token has invalid" });

    req.user = dataFromToken.data;
    req.token = jwt.sign({ data: dataFromToken.data }, process.env.TOKEN, {
      expiresIn: "24h",
    });

    next();
  },
};
