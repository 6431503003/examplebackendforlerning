// Define middleware function to check user role
function authRole(role) {
    return (req, res, next) => {
        // Check if the session variable corresponding to the role exists
        if (req.session && req.session[role]) {
            // User has the required role, proceed to the next middleware
            next();
        } else {
            // User does not have the required role, respond with an error
            res.status(403).json({ error: "Unauthorized" });
        }
    };
}

module.exports = authRole;