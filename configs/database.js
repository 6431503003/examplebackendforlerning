const mysql = require('mysql2')
const dotenv = require('dotenv')
dotenv.config();

const connection = mysql.createConnection(process.env.database, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
});

connection.connect((err) => {
    if (err) {
        console.error(`DB Connection Error: ${err.message}`);
        return;
    }
    console.log('DB Connected!');
});

module.exports = connection;

