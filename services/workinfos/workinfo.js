const connection = require("../../configs/database");

module.exports = {
  async findALL() {
    return new Promise((resolve, reject) => {
      let sql = "SELECT * FROM workinfo";
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async getById(id) {
    return new Promise((resolve, reject) => {
      let sql = "SELECT * FROM workinfo WHERE user_id = ?";
      connection.query(sql, id, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },
  async getTimeAttByDate(id, formatDateForworkinfo) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT * FROM workinfo WHERE user_id = ? AND dateCheckIn LIKE '%${formatDateForworkinfo}%';`
      connection.query(sql, id, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async getByMonth(id, month) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT *
        FROM workinfo
        WHERE user_id = ${id}
          AND YEAR(STR_TO_DATE(dateCheckIn, '%m/%d/%Y')) = YEAR(CURDATE())
          AND MONTH(STR_TO_DATE(dateCheckIn, '%m/%d/%Y')) = ${month -1};`;
        let currentYear = new Date().getFullYear(); // Get the current year
        connection.query(sql, [id, month, currentYear], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
},



  
  async getByIdandsorthightolow(id) {
    return new Promise((resolve, reject) => {
      let sql =
        "SELECT * FROM workinfo WHERE user_id = ? ORDER BY dateCheckIn DESC;";
      connection.query(sql, id, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },
  async getById(id) {
    return new Promise((resolve, reject) => {
      let sql = "SELECT * FROM workinfo WHERE user_id = ?";
      connection.query(sql, id, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async create(workinfo) {
    try {
      return await new Promise((resolve, reject) => {
        let sql = "INSERT INTO workinfo SET ?";
        connection.query(sql, workinfo, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    } catch (err) {
      throw err;
    }
  },

  //Todo: make it send to query
  //maybe it will work im not sure
  async createCheckIn(workinfo) {
    try {
      return await new Promise((resolve, reject) => {
        // console.log('form service userid = ',workinfo.user_id)
        let findCheckoutQuery = `
                SELECT user_id, dateCheckIn, DATE(dateCheckOut) AS dateCheckOut
                FROM workinfo
                WHERE DATE(STR_TO_DATE(dateCheckIn, '%m/%d/%Y')) = DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))
                AND DATE(dateCheckOut) IS NULL
                AND user_id = ?`;

        connection.query(
          findCheckoutQuery,
          [workinfo.user_id],
          (err, result) => {
            if (err) {
              // Handle query execution error
              reject(err);
            } else if (result.length === 0) {
              // Condition not met, reject with an error message
              resolve({ message: "Checkout first" });
            } else {
              let insertCheckInQuery = "INSERT INTO workinfo SET ?";
              connection.query(insertCheckInQuery, workinfo, (err, result) => {
                if (err) {
                  // Handle query execution error
                  reject(err);
                } else {
                  // Resolve with the result of the INSERT query
                  resolve(result);
                }
              });
            }
          }
        );
      });
    } catch (err) {
      throw err;
    }
  },

  async update(id, workinfo) {
    return new Promise((resolve, reject) => {
      let sql = "UPDATE workinfo SET ? WHERE workinfo_id = ?";
      connection.query(sql, [workinfo, id], (err, result) => {
        if (err) reject(err);
        resolve(result);
      });
    });
  },

  async checkOut(id, workinfo) {
    return new Promise((resolve, reject) => {
      let sql =
        "UPDATE workinfo SET ? WHERE user_id = ? AND dateCheckOut IS NULL";
      connection.query(sql, [workinfo, id], (err, result) => {
        if (err) reject(err);
        resolve(result);
      });
    });
  },

  async delete(id) {
    return new Promise((resolve, reject) => {
      let sql = "DELETE FROM workinfo WHERE user_id = ?";
      connection.query(sql, id, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async findTodayAtt(todayDate) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT * FROM workinfo WHERE dateCheckIn LIKE '%${todayDate}%' `;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async joinAtt(today, formatDateForworktype) {
    return new Promise((resolve, reject) => {
      let sql = `
      SELECT 
          users.user_id, users.firstName, users.lastName, users.empId, users.department, users.subDepartment,
          workinfo.user_id, workinfo.timeCheckIn, workinfo.timeCheckOut, workinfo.workplace, 
          workinfo.noteIn, workinfo.noteOut, workinfo.dateCheckIn, workinfo.dateCheckOut, 
          worktypeData.date, worktypeData.workType, workinfo.imageIn, workinfo.imageOut
      FROM 
          users
      INNER JOIN 
          workinfo ON users.user_id = workinfo.user_id
      INNER JOIN 
          worktypeData ON users.user_id = worktypeData.user_id
      WHERE 
            workinfo.dateCheckIn LIKE '%${today}%'
      AND
            worktypeData.date LIKE '%${formatDateForworktype}'
      ;`;

      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },
  async joinAbsence(today) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT users.user_id, users.line_userId, users.firstName, users.lastName, users.empId, users.profilePic,users.department,users.subDepartment
      FROM users
      LEFT JOIN workinfo ON users.user_id = workinfo.user_id AND workinfo.dateCheckIn LIKE '%${today}%'
      WHERE workinfo.user_id IS NULL 
      AND users.account_status = 'accept';`;

      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async individualWorktimeCal(id, month, year) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT users.clockIn, users.clockOut, workinfo.timeCheckIn, workinfo.timeCheckOut,
      workinfo.workplace, workinfo.workinfo_id, workinfo.dateCheckIn, workinfo.dateCheckOut, 
      workinfo.noteIn, workinfo.noteOut, workinfo.imageIn, workinfo.imageOut, workinfo.isLate, workinfo.lateTime
            FROM users
            INNER JOIN workinfo ON users.user_id = workinfo.user_id
            WHERE users.user_id = ${id}
            AND workinfo.dateCheckIn LIKE '%${month}/%/${year}%'
            AND workinfo.timeCheckIn IS NOT NULL
            AND workinfo.timeCheckOut IS NOT NULL
            AND workinfo.dateCheckIn IS NOT NULL
            AND workinfo.dateCheckOut IS NOT NULL          
            `;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },
  //dashboard page
async getattendanceStatus(today,formatDateForworktype){
  return new Promise((resolve, reject) => {
    let sql = `SELECT users.user_id, users.line_userId, users.clockIn, users.clockOut,users.nameTitle, users.firstName, users.lastName,
    users.empId, users.department, users.subDepartment,
   workinfo.user_id, workinfo.workinfo_id, workinfo.timeCheckIn, workinfo.timeCheckOut, workinfo.dateCheckIn, workinfo.dateCheckOut, workinfo.workplace, 
   workinfo.noteIn, workinfo.noteOut, workinfo.dateCheckIn, workinfo.dateCheckOut, 
   workinfo.isLate, workinfo.lateTime,
   worktypeData.date, worktypeData.workType, workinfo.imageIn, workinfo.imageOut
FROM 
   users
LEFT JOIN 
   workinfo 
ON 
   users.user_id = workinfo.user_id
   AND workinfo.dateCheckIn LIKE '%${today}%'
LEFT JOIN 
   worktypeData 
ON 
   users.user_id = worktypeData.user_id
   AND worktypeData.date LIKE '%${formatDateForworktype}' 
WHERE
   users.account_status = 'accept';
 ;`;
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
},

  async todayWorkinfo() {
    return new Promise((resolve, reject) => {
      let sql = `SELECT w.user_id, u.firstName, u.lastName, u.empId, u.subDepartment, u.profilePic, w.dateCheckIn, w.dateCheckOut
        FROM workinfo w
        INNER JOIN users u ON w.user_id = u.user_id
        WHERE DATE(STR_TO_DATE(w.dateCheckIn, '%m/%d/%Y')) = CURDATE();
        `;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async todayLateWorkinfo() {
    return new Promise((resolve, reject) => {
      let sql = `SELECT u.user_id, u.empId, u.firstName, u.lastName,u.subDepartment, u.clockIn,u.profilePic, w.timeCheckIn, w.dateCheckIn
      FROM users u 
      INNER JOIN workinfo w ON u.user_id = w.user_id
      WHERE w.timeCheckIn > u.clockIn AND DATE(STR_TO_DATE(dateCheckIn, '%m/%d/%Y')) = CURDATE();
      `;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  //dashboard page
  //officer
  async checkInCurrentWeek(id) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT user_id, dateCheckIn
                FROM workinfo
                WHERE user_id = ${id}
                  AND STR_TO_DATE(dateCheckIn, '%m/%d/%Y') >= CURDATE() - INTERVAL WEEKDAY(CURDATE()) DAY  -- Monday of the current week
                  AND STR_TO_DATE(dateCheckIn, '%m/%d/%Y') <= CURDATE() + INTERVAL 4 - WEEKDAY(CURDATE()) DAY; -- Friday of the current week`;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  async workinfobyyear(id, year) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT w.workplace, 
      w.timeCheckIn, 
      w.timeCheckOut, 
      TIMEDIFF(w.timeCheckOut, w.timeCheckIn) AS totalWorkTime, 
      w.dateCheckOut, 
      w.dateCheckIn 
      FROM workinfo w 
      WHERE w.user_id = ${id}
      AND YEAR(STR_TO_DATE(w.dateCheckIn, '%m/%d/%Y')) = ${year};`;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },
  async workinfobyid(id) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT workplace FROM workinfo WHERE user_id = ${id} AND YEAR(date) = ${selectedYear} AND workplace = "WFH";`;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  },

  //officer

  // async individualWorktimeData(id) {
  //     return new Promise((resolve,reject) => {
  //         let sql =
  //         `SELECT users.clockIn, users.clockOut, workinfo.timeCheckIn, workinfo.timeCheckOut, workinfo.workplace, workinfo.dateCheckIn, workinfo.dateCheckOut
  //         FROM users
  //         INNER JOIN workinfo ON users.user_id = workinfo.user_id
  //         WHERE users.user_id = ${id}  AND workinfo.dateCheckIn LIKE '%${date}%'`
  //         connection.query(sql,
  //             (err, result) => {
  //             if (err) {
  //                 reject(err);
  //             } else {
  //                 resolve(result);
  //             }
  //         });
  //     })
  // },
};
