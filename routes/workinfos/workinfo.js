const router = require('express').Router();
const service = require('../../services/workinfos/workinfo');

router.get('/getALL', async (req, res) => {
    console.log("Get all of workinfo data")
    try {
        const items = await service.findALL();
        console.log("Get all of workinfo data successfully")
        // console.log(items)

        if (items.length == 0) {
            console.log("Not found when get by workinfo data");
            res.status(404).send({
                "status": 404,
                "message": "Not found workinfo data"
            })
        } else {
            res.send(items)
        }
    } catch (error) {
        console.log("Error when get by id of workinfo :", error)
        res.status(500).send(error)

    }
})
router.get('/getAttendanceData', async (req, res) => {
    console.log("Get all of workinfo data")
    try {
        // const today = new Date().toISOString().slice(0, 10);
        const clockInDate = new Date();
        function formatDate(clockInDate) {
            const day = String(clockInDate.getDate()).padStart(2, '0');
            const month = String(clockInDate.getMonth() + 1).padStart(2, '0');
            const year = clockInDate.getFullYear();
          
            return `${month}/${day}/${year}`;
          }
          function formatDateForworktype(clockInDate) {
            const day = String(clockInDate.getDate()).padStart(2, '0');
            const month = String(clockInDate.getMonth() + 1).padStart(2, '0');
            const year = clockInDate.getFullYear();
          
            return `${year}-${month}-${day}`;
          }
          
              const formattedDateIn = formatDate(clockInDate);
              const formattedDateIn2 = formatDateForworktype(clockInDate);

        const items = await service.getattendanceStatus(formattedDateIn,formattedDateIn2);
        // console.log(items)

        if (items.length == 0) {
            console.log("Not found when get by workinfo data");
            res.status(404).send({
                "status": 404,
                "message": "Not found workinfo data"
            })
        } else {
            res.send(items)
        }
    } catch (error) {
        console.log("Error when get by id of workinfo :", error)
        res.status(500).send(error)

    }
})
router.get('/getAbsenceData', async (req, res) => {
    console.log("Get all of workinfo data")
    try {
        // const today = new Date().toISOString().slice(0, 10);
        const clockInDate = new Date();
        function formatDate(clockInDate) {
            const day = String(clockInDate.getDate()).padStart(2, '0');
            const month = String(clockInDate.getMonth() + 1).padStart(2, '0');
            const year = clockInDate.getFullYear();
          
            return `${month}/${day}/${year}`;
          }
          
              const formattedDateIn = formatDate(clockInDate);
        const items = await service.joinAbsence(formattedDateIn);
        // console.log(items)

        if (items.length == 0) {
            console.log("Not found when get by workinfo data");
            res.status(404).send({
                "status": 404,
                "message": "Not found workinfo data"
            })
        } else {
            res.send(items)
        }
    } catch (error) {
        console.log("Error when get by id of workinfo :", error)
        res.status(500).send(error)

    }
})
//dashboard page
router.get('/dashboard/getTodayData',async (req, res) => {
    console.log("Get all of today workinfo data")
    try {
        const items = await service.todayWorkinfo();
        console.log("Get all of workinfo data successfully")
        // console.log(items)

        if (items.length == 0) {
            console.log("Not found when get by workinfo data");
            res.status(404).send({
                "status": 404,
                "message": "Not found workinfo data"
            })
        } else {
            res.send(items)
        }
    } catch (error) {
        console.log("Error when get by id of workinfo :", error)
        res.status(500).send(error)

    }
});

router.get('/dashboard/getLateData',async (req, res) => {
    console.log("Get all late users today")
    try {
        const items = await service.todayLateWorkinfo();
        console.log("Get all of workinfo data successfully")
        // console.log(items)

        if (items.length == 0) {
            console.log("Not found when get by workinfo data");
            res.status(404).send({
                "status": 404,
                "message": "Not found workinfo data"
            })
        } else {
            res.send(items)
        }
    } catch (error) {
        console.log("Error when get by id of workinfo :", error)
        res.status(500).send(error)

    }
});

//dashboard page

router.get('/getTodayAtt', async (req, res) => {
    try {
        const clockInDate = new Date();
        function formatDate(clockInDate) {
            const day = String(clockInDate.getDate()).padStart(2, '0');
            const month = String(clockInDate.getMonth() + 1).padStart(2, '0');
            const year = clockInDate.getFullYear();
          
            return `${month}/${day}/${year}`;
          }
          
              const formattedDateIn = formatDate(clockInDate);
        const items = await service.joinAtt(formattedDateIn);
        // console.log(items)
        let result = await service.findTodayAtt(clockInDate);
        if (result !== null) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

router.get('/getattendanceStatus', async (req, res) => {
    try {
        const date = new Date();
        function formatDate(date) {
            const day = String(date.getDate()).padStart(2, '0');
            const month = String(date.getMonth() + 1).padStart(2, '0');
            const year = date.getFullYear();
          
            return `${month}/${day}/${year}`;
          }
          const today = formatDate(date);

        const dates = new Date();
        function formatDatedash(dates) {
            const day = String(dates.getDate()).padStart(2, '0');
            const month = String(dates.getMonth() + 1).padStart(2, '0');
            const year = dates.getFullYear();
          
            return `${year}-${month}-${day}`;
          }
          const formatDateForworktype = formatDatedash(dates);

        //   console.log("today/ ",today);
        //   console.log("format- ",formatDateForworktype);
        let result = await service.getattendanceStatus(today, formatDateForworktype);
        
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});
//dashboard page

//officer
router.get('/getcheckin-currentweek/:id', async (req, res) => {
    try {
        let result = await service.checkInCurrentWeek(req.params.id);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

router.get('/getworkinfobyyear/:id', async (req, res) => {
    try {
      const userId = req.params.id;
      const year = req.query.year;
      const result = await service.workinfobyyear(userId, year);
      if (result.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(404).send('No work information found for the given year');
      }
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  });
  //officer work sum get wfh
  router.get('/getworkinfobyid/:id', async (req,res) =>{
    try {
        let result = await service.workinfobyid(req.params.id);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
})


router.get('/get/:id/:month', async (req, res) => {
    try {
        let result = await service.getByMonth(req.params.id, req.params.month);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});


  

//officer


router.get('/get/:id', async (req, res) => {
    try {
        // console.log(req.params.id);
        
        let result = await service.getById(req.params.id);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});
router.get('/getTodayAtten/:id', async (req, res) => {
    try {
        
        const date = new Date();
        function formatDate(dates) {
            const day = String(dates.getDate()).padStart(2, '0');
            const month = String(dates.getMonth() + 1).padStart(2, '0');
            const year = dates.getFullYear();
          
            return `${month}/${day}/${year}`;
          }
          const formatDateForworkinfo = formatDate(date);
        //   console.log('getTodayAtten of ',req.params.id + "date = ",formatDateForworkinfo);
        let result = await service.getTimeAttByDate(req.params.id, formatDateForworkinfo);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});
router.get('/getandsort/:id', async (req, res) => {
    try {
        // console.log(req.params.id);
        let result = await service.getByIdandsorthightolow(req.params.id);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});
router.get('/getWorktimeCal/:id/:month/:year', async (req, res) => {
    const {id, month, year} = req.params
    try {
        // console.log("getWorktimeCal got ",req.params);
        let result = await service.individualWorktimeCal(id, month, year);
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).send('Item not found');
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

// router.get('/getWorkData/:id', async (req, res) => {
//     try {
//         console.log(req.params.id);
//         let result = await service.individualWorktimeData(req.params.id);
//         if (result) {
//             res.status(200).json(result);
//         } else {
//             res.status(404).send('Item not found');
//         }
//     } catch (err) {
//         res.status(500).json({ error: err.message });
//     }
// });

router.post('/create', async (req, res) => {
    try {
        let result = await service.create(req.body);
        res.status(200).json(result);
    } catch (err) {
        console.error(JSON.stringify(err));
        res.status(500).json({ error: err.message });
    }
});

router.post('/checkIn', async (req, res) => {
    try {
        let result = await service.createCheckIn(req.body);
        // console.log(result)
        res.status(200).json(result);
    } catch (err) {
        console.error(JSON.stringify(err));
        res.status(500).json({ error: err.message });
    }
});

router.put('/update/:id', async (req, res) => {
    try {
        let result = await service.update(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});
router.put('/checkOut/:id', async (req, res) => {
    try {
        let result = await service.checkOut(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

router.delete('/delete/:id', async (req, res) => {
    try {
        let result = await service.delete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
});

module.exports = router