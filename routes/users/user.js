const router = require("express").Router();
const service = require("../../services/users/user");
const workinfoService = require("../../services/workinfos/workinfo");
const axios = require("axios");
const bcrypt = require("bcrypt");
const authRole = require('../../configs/auth')
const {
  passwordGenerator,
  tokenGenerator,
  validUser,
  reactiveUser,
} = require('../../security/index')
const CHANNEL_ACCESS_TOKEN = process.env.chanelToken;

const log4js = require("log4js");
log4js.configure("./configs/log4js.json");
const log = log4js.getLogger("server");

router.post("/login", async (req, res) => {
  try {
    const { empId, password } = req.body;
    const userNotHashPass = await service.login(empId , password)
    
    const user = await service.findByEmpId(empId);
    console.log('user password: ',user);
    if (!user) {
      console.log("Not found empId", empId);
      return res.status(404).send({ message: "Not found user" });
    }

    const isPasswordMatch = await bcrypt.compare(password, user[0].password)
    if (!isPasswordMatch) {
      return res.status(401).send({ message: "Incorrect empId or password" });
    }

    const items = await service.login(empId, user[0].password);
    console.log(items[0].role);
    if (items.length === 0) {
      console.log("Not found empId ", empId);
      res.status(404).send({ message: "Not found user" });
    } else {
      let user = items[0];
      switch (user.role) {
        case "admin":
          req.session.admin = user;
          break;
        case "manager":
          req.session.manager = user;
          break;
        case "officer":
          req.session.officer = user;
          break;
        default:
          res.status(500).send("Unexpected user role");
          return;
      }
      //   //monitor
      //   let userRole;
      //   if (req.session.admin) {
      //     userRole = "admin";
      //   } else if (req.session.manager) {
      //     userRole = "manager";
      //   } else if (req.session.officer) {
      //     userRole = "officer";
      //   } else {
      //     // This should not happen if the switch statement is properly handled
      //     userRole = "unknown";
      //   }

      //   // Redirect or send response as needed
      //   res.send(`Login successful. User role: ${userRole}`);
      let token = tokenGenerator(user);
      user.token = token;
      res.send(user);
    }
  } catch (error) {
    console.log("Error when insert a new data of users : ", error);
    res.status(500).send(error);
  }
});

router.post('/logout', (req, res) => {
    // Check if user is logged in (session exists)
    if (req.session) {
        // Destroy the session
        req.session.destroy(err => {
            if (err) {
                console.error('Error destroying session:', err);
                res.status(500).send('Internal Server Error');
            } else {
                // Redirect or send response indicating successful logout
                res.send('Logout successful');
            }
        });
    } else {
        // If no session exists, send response indicating user is not logged in
        res.status(401).send('User is not logged in');
    }
});

//ยังไม่มีการเรียกใช้
router.post("/register", async (req, res) => {
  try {
    const items = {
      lineName: req.body.lineName,
      // linePictureUrl : req.body._value.linePictureUrl, //Open when system ready now just for test
      line_userId: req.body.line_userId,
      // email : req.body._value.email, //Open when system ready now just for test
    };
    // console.log("this is line_userId : ",items.line_userId);
    const checkuserid = await service.findBylineUserId(items.line_userId);
    if (checkuserid.length == 0) {
      const data = service.register(items);
      res.status(200).json(data);
      console.log("Push user data successfully");
    } else {
      res.status(500).json("Already Have your data in database");
    }
  } catch (error) {
    console.log("Fail to psuh register data :", error);
    res.status(500).send(error);
  }
});

router.get("/getALL",async (req, res) => {
  console.log("Get all of users data");
  try {
    const items = await service.findALL();
    console.log("Get all of user data successfully");
    console.log(items);

    if (items.length == 0) {
      console.log("Not found when get by user data");
      res.status(404).send({
        status: 404,
        message: "Not found user data",
      });
    } else {
      res.send(items);
    }
  } catch (error) {
    console.log("Error when get by id of user :", error);
    res.status(500).send(error);
  }
});

router.get("/getEmpID", async (req, res) => {
  console.log("Get all of users data");
  try {
    const items = await service.findEmpId();
    log.info("Get all of empID successfully");
    console.log(items);

    if (items.length == 0) {
      log.warning("Not found empID data");
      res.status(404).send({
        status: 404,
        message: "Not found empID data",
      });
    } else {
      res.send(items);
    }
  } catch (error) {
    log.error("Error when get empID :", error);
    res.status(500).send(error);
  }
});

//TEST Line push message

router.get("/getLineId", async (req, res) => {
  console.log("Get all line Id of users data");
  try {
    const items = await service.getLineId();
    console.log("Get all of user data successfully");
    console.log(items);

    if (items.length == 0) {
      console.log("Not found when get by user data");
      res.status(404).send({
        status: 404,
        message: "Not found user data",
      });
    } else {
      res.send(items);
    }
  } catch (error) {
    console.log("Error when get by id of user :", error);
    res.status(500).send(error);
  }
});

router.get("/getUserId/:lineuser_Id", async (req, res) => {
  console.log("Get all line Id of users data");
  try {
    console.log("lineid = ", req.params);
    const items = await service.getUserIdbyLineId(req.params.lineuser_Id);
    console.log("Get all of user data successfully");
    console.log(items);

    if (items.length == 0) {
      console.log("Not found when get by user data");
      res.status(404).send({
        status: 404,
        message: "Not found user data",
      });
    } else {
      res.send(items);
    }
  } catch (error) {
    console.log("Error when get by id of user :", error);
    res.status(500).send(error);
  }
});

router.get("/get/:id", async (req, res) => {
  try {
    let result = await service.getById(req.params.id);
    if (result) {
      res.status(200).json(result);
    } else {
      res.status(404).send("Item not found");
    }
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.get("/getPending", async (req, res) => {
  try {
    let result = await service.getPendingOfficer();
    if (result) {
      res.status(200).json(result);
    } else {
      res.status(404).send("Item not found");
    }
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.get("/getOfficer", async (req, res) => {
  try {
    let result = await service.getOfficer();
    if (result) {
      res.status(200).json(result);
    } else {
      res.status(404).send("Item not found");
    }
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.get("/getAdmin", async (req, res) => {
  try {
    let result = await service.getAdmin();
    if (result) {
      res.status(200).json(result);
    } else {
      res.status(404).send("Item not found");
    }
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.post("/create", async (req, res) => {
  try {
     // Hash the password before storing it
     const hashedPassword = await bcrypt.hash(req.body.password, 10);

    const user = {
      // name: req.body.name,
      nameTitle: req.body.nameTitle,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      empId: req.body.empId,
      password: hashedPassword,
      department: req.body.department,
      subDepartment: req.body.subDepartment,
      email: req.body.email,
      clockIn: req.body.clockIn,
      clockOut: req.body.clockOut,
      role: req.body.role,
      account_status: req.body.account_status,
      line_userId: req.body.line_userId,
      lineName: req.body.lineName,
      //homeLocation: JSON.stringify(req.body.homeLocation)
    };

    let result = await service.create(user);
    res.status(200).json(result);
  } catch (err) {
    console.error(JSON.stringify(err));
    res.status(500).json({ error: err.message });
  }
});

router.put("/update/:id", async (req, res) => {
  try {
    console.log(req.body);
    let result = await service.update(req.params.id, req.body);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/update/homeLocation1/:id", async (req, res) => {
  try {
    const user = {
      homeLocation1: JSON.stringify(req.body.homeLocation1),
    };
    let result = await service.update(req.params.id, user);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/delete/homeLocation1/:id", async (req, res) => {
  try {
    let result = await service.deleteHome1(req.params.id);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/update/homeLocation2/:id", async (req, res) => {
  try {
    const user = {
      homeLocation2: JSON.stringify(req.body.homeLocation2),
    };
    let result = await service.update(req.params.id, user);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/delete/homeLocation2/:id", async (req, res) => {
  try {
    let result = await service.deleteHome2(req.params.id);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/update/homeLocation3/:id", async (req, res) => {
  try {
    const user = {
      homeLocation3: JSON.stringify(req.body.homeLocation3),
    };
    let result = await service.update(req.params.id, user);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/delete/homeLocation3/:id", async (req, res) => {
  try {
    let result = await service.deleteHome3(req.params.id);
    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.put("/updateByEmp", async (req, res) => {
  console.log("body = ", req);
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    let result = await service.updateByEmp(hashedPassword, req.body.empId);

    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

router.delete("/delete/:id", async (req, res) => {
  try {
    await workinfoService.delete(req.params.id).then(() => {
      let result = service.delete(req.params.id);
      res.status(200).json(result);
    });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Send Line Message
router.post("/proxy-line-api", async (req, res) => {
  try {
    // Forward the request to the LINE Messaging API
    const message = {
      to: req.body.line_userId,
      messages: [
        {
          type: "text",
          text: "Hello from your application!", //mockup
        },
      ],
    };
    const response = await axios.post(
      "https://api.line.me/v2/bot/message/push",
      message,
      {
        headers: {
          Authorization: `Bearer ${CHANNEL_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    // Return the response from the LINE Messaging API to the client
    res.json(response.data);
  } catch (error) {
    console.error("Error proxying request to LINE API:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
router.post("/proxy-line-api/registered", async (req, res) => {
  try {
    console.log("req = ", req.body.line_userId);
    const message = {
      to: req.body.line_userId,
      messages: [
        {
          type: "text",
          text: "Register completed. Please wait for admin approval.",
        },
      ],
    };
    const response = await axios.post(
      "https://api.line.me/v2/bot/message/push",
      message,
      {
        headers: {
          Authorization: `Bearer ${CHANNEL_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    res.json(response.data);
  } catch (error) {
    console.error("Error proxying request to LINE API:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
router.post("/proxy-line-api/approve", async (req, res) => {
  try {
    const message = {
      to: req.body.line_userId,
      messages: [
        {
          type: "text",
          text: "You are approved. Please try logging in to our website.",
        },
      ],
    };
    const response = await axios.post(
      "https://api.line.me/v2/bot/message/push",
      message,
      {
        headers: {
          Authorization: `Bearer ${CHANNEL_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    res.json(response.data);
  } catch (error) {
    console.error("Error proxying request to LINE API:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
router.post("/proxy-line-api/checkin", async (req, res) => {
  try {
    const clockInDate = new Date();
    function formatDate(clockInDate) {
      const hours = String(clockInDate.getHours()).padStart(2, "0");
      const minutes = String(clockInDate.getMinutes()).padStart(2, "0");

      const day = String(clockInDate.getDate()).padStart(2, "0");
      const month = String(clockInDate.getMonth() + 1).padStart(2, "0");
      const year = clockInDate.getFullYear();

      return `${hours}:${minutes} - ${month}/${day}/${year}`;
    }

    const formattedDateIn = formatDate(clockInDate);
    console.log("id checkin ", req.body);
    const message = {
      to: req.body.line_userId,
      messages: [
        {
          type: "text",
          text: `Check-In completed : ${formattedDateIn}`,
        },
      ],
    };
    const response = await axios.post(
      "https://api.line.me/v2/bot/message/push",
      message,
      {
        headers: {
          Authorization: `Bearer ${CHANNEL_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    res.json(response.data);
  } catch (error) {
    console.error("Error proxying request to LINE API:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
router.post("/proxy-line-api/checkout", async (req, res) => {
  try {
    const clockOutDate = new Date();
    function formatDate(clockOutDate) {
      const hours = String(clockOutDate.getHours()).padStart(2, "0");
      const minutes = String(clockOutDate.getMinutes()).padStart(2, "0");

      const day = String(clockOutDate.getDate()).padStart(2, "0");
      const month = String(clockOutDate.getMonth() + 1).padStart(2, "0");
      const year = clockOutDate.getFullYear();

      return `${hours}:${minutes} - ${month}/${day}/${year}`;
    }

    const formattedDateOut = formatDate(clockOutDate);
    const message = {
      to: req.body.line_userId,
      messages: [
        {
          type: "text",
          text: `Check-Out completed : ${formattedDateOut}`,
        },
      ],
    };
    const response = await axios.post(
      "https://api.line.me/v2/bot/message/push",
      message,
      {
        headers: {
          Authorization: `Bearer ${CHANNEL_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    res.json(response.data);
  } catch (error) {
    console.error("Error proxying request to LINE API:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/proxy-line-api/normalMsg/:id", async (req, res) => {
  try {
    const message = {
      to: req.params.id,
      messages: [
        {
          type: "text",
          text: req.body.msg,
        },
      ],
    };
    const response = await axios.post(
      "https://api.line.me/v2/bot/message/push",
      message,
      {
        headers: {
          Authorization: `Bearer ${CHANNEL_ACCESS_TOKEN}`,
          "Content-Type": "application/json",
        },
      }
    );

    res.json(response.data);
  } catch (error) {
    console.error("Error proxying request to LINE API:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
module.exports = router;
