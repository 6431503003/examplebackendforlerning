const express = require('express')
const cors = require('cors')
const server = express()
const routesIndex = require('./routes/IndexRoutes')
const lineroute = require('./routes/lineRoutes')
const session = require('express-session');
// const {startScheduler} = require('./scheduler/officer_scheduler');
const dotEnv = require("dotenv")
dotEnv.config()

// startScheduler();

const log4js = require('log4js')
const { start } = require('pm2')
log4js.configure('./configs/log4js.json');
const log = log4js.getLogger('server')

server.use(session({
    secret: process.env.SECRET_TOKEN,
    resave: false,
    saveUninitialized: true
}));
server.use(cors());

//ปกติแล้วพี่จะใช้อันที่commentไว้ แต่projectนี้ต้องใช้แบบที line 30
// server.use(express.json())
// server.use(express.urlencoded({ extended: true }))
// server.use(express.static('public'))
 
server.use('/api',express.json(),express.urlencoded({ extended: true }),express.static('public'),routesIndex)
server.use('/line',lineroute)

server.get('/', (req, res) => {
    res.json({ meg: 'hello world' });
})

server.listen(process.env.PORT, () => {
    log.info('Server started : ', process.env.PORT)
    // console.log('Server started : ', process.env.PORT);
})

