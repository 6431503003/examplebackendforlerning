const { response } = require('express');
const connection = require('../../configs/database')
// const bcrypt = require('bcrypt');

module.exports = {
    async login(empId , password) {
        // console.log(empId , password)
        // console.log("empId form service = ",empId)
        // console.log("pass form service = ",password)
        const sql = "SELECT * FROM users WHERE empId = ? AND password = ?"
        
        return new Promise((resolve, reject) => {
          connection.query(sql, [empId, password], async (err, results) => {
            if (err) {
              reject(err);
            } else {
              resolve(results);
            }
          });
        });
    },

    async register(items) {
        try {
            return await new Promise((resolve, reject) => {
                let sql = "INSERT INTO users SET ?";
                connection.query(sql, items, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            });
        } catch (err) {
            throw err;
        }
    },

    async findALL() {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM users";
            connection.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async findEmpId() {
        return new Promise((resolve, reject) => {
            let sql = "SELECT empId FROM users";
            connection.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async findByEmpId(empId) {
        return new Promise((resolve, reject) => {
            let sql = `SELECT password FROM users where empId = ${empId}`;
            connection.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async findBylineUserId(userid) {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM users WHERE line_userId = ?";
            connection.query(sql, userid,(err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async getLineId() {
        return new Promise((resolve, reject) => {
            // let sql = "select line_userId from users where line_userId IS NOT NULL";
            let sql = "select line_userId from users";
            connection.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async getUserIdbyLineId(user_id) {
        return new Promise((resolve, reject) => {

            let sql = "select user_id, clockIn from users where line_userId = ?";
            connection.query(sql,user_id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    },

    async getById(id) {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM users WHERE user_id = ?";
            connection.query(sql, id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },
    
    async getPendingOfficer(acc_status) {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM users WHERE account_status = 'pending'";
            connection.query(sql, acc_status, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async getOfficer(acc_status) {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM users WHERE account_status = 'accept'";
            connection.query(sql, acc_status, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async getAdmin(role) {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM users WHERE role = 'admin'";
            connection.query(sql, role, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async create(user) {
        try {
            return await new Promise((resolve, reject) => {
                let sql = "INSERT INTO users SET ?";
                connection.query(sql, user, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            });
        } catch (err) {
            throw err;
        }
    },

    async update(id, user) {
        return new Promise((resolve, reject) => {
            // const { homeLocation } = user;
            console.log("This is user = ",user);
            let sql = "UPDATE users SET ? WHERE user_id = ?";
            connection.query(sql, [user, id], (err, result) => {
                if (err) {
                    console.error(err);
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    },

    async updateByEmp(password, empId) {
        console.log("password = ",password);
        console.log("empId = ",empId);
        return new Promise((resolve, reject) => {
            let sql = "UPDATE users SET password = ? WHERE empId = ?";
            connection.query(sql, [password, empId], (err, result) => {
                if (err) {
                    console.error(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => err(err))
    },

    async delete(id) {
        return new Promise((resolve, reject) => {
            let sql = "DELETE FROM users WHERE user_id = ?";
            connection.query(sql, id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async deleteHome1(id) {
        return new Promise((resolve, reject) => {
            let sql = 'UPDATE users SET homeLocation1 = NULL WHERE user_id = ?';
            connection.query(sql, id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async deleteHome2(id) {
        return new Promise((resolve, reject) => {
            let sql = 'UPDATE users SET homeLocation2 = NULL WHERE user_id = ?';
            connection.query(sql, id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },

    async deleteHome3(id) {
        return new Promise((resolve, reject) => {
            let sql = 'UPDATE users SET homeLocation3 = NULL WHERE user_id = ?';
            connection.query(sql, id, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch((err) => reject(err))
    },
}